def scan(userwords)
  sentence = Array.new
  userwords.each { |word|
    if word.include?("north", "south", "east", "west")
      sentence.push(Pair.new(:direction, word))
  }
  sentence
end 